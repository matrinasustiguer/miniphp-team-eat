<?php

require "template/template.php";

function getTitle()
{
  echo "TaskaHolic";
}

function getContent()
{

  require "controllers/connection.php";

?>

  <div class="containerMission">
    <div class="row">

      <div class="col-lg-3">
        <h3 class="py-4 text-center">Add New Task</h3>
        <form action="controllers/process_add_todo.php" method="POST" class="mb-5" enctype="multipart/form-data">
          <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" class="form-control" placeholder="Input Task Name">
          </div>
          <div class="form-group">
            <label for="priority_level">Priority Level:</label>
            <select name="priority_level" class="form-control">
              <option value="1">Normal</option>
              <option value="2">Urgent</option>
              <option value="3">Very Urgent</option>
            </select>
          </div>
          <div class="form-group">
            <label for="details">Task Details:</label>
            <textarea name="details" class="form-control" rows="8" cols="30"></textarea>
          </div>

          <input type="hidden" name="status_id" value="1">

          <button class="btn btn-info" type="submit">Add</button>
        </form>
      </div>

      <div class="col-lg-9">
        <h5 class="my-2 text-center">Status</h5>
        <ul class="list-group border list-group-horizontal">
          <li class="list-group-item flex-fill p-1 text-center">
            <a href="missions.php">All</a>
          </li>

          <?php
          $status_query = "SELECT * FROM statuses";
          $statuses = mysqli_query($conn, $status_query);

          foreach ($statuses as $indivStatus) {
          ?>
            <li class="list-group-item flex-fill p-1 text-center">
              <a href="missions.php?status_id=<?php echo $indivStatus['id'] ?>"><?php echo $indivStatus['name']; ?></a>
            </li>
          <?php
          }
          ?>
        </ul>

        <h5 class="my-2 text-center">Sort By</h5>
        <ul class="list-group border list-group-horizontal">
          <li class="list-group-item flex-fill p-1 text-center">
            <a href="controllers/process_sort.php?sort=asc">Priority Level (Normal to Very Urgent)</a>
          </li>
          <li class="list-group-item flex-fill p-1 text-center">
            <a href="controllers/process_sort.php?sort=desc">Priority Level (Very Urgent to Normal)</a>
          </li>
        </ul>

        <h1 class="text-center py-4">Your Tasks</h1>
        <div class="row">

          <?php
          $user_id = $_SESSION['user']['id'];

          $todos_query = "SELECT * FROM todos WHERE user_id = $user_id ";

          if (isset($_GET['status_id'])) {
            $statusId = $_GET['status_id'];


            $todos_query .= " AND status_id = $statusId";
          }

          if (isset($_SESSION['sortDataFromSession'])) {
            $todos_query .= $_SESSION['sortDataFromSession'];
          }

          $todos = mysqli_query($conn, $todos_query);

          foreach ($todos as $indivTodo) {
          ?>
            <div class="col-lg-4 py-2">
              <div class="card text-white bg-dark">
                <div class="card-body">
                  <h4 class="card-title"><?php echo $indivTodo['title']  ?></h4>

                  <p class="card-text">
                    Priority Level:
                    <?php if ($indivTodo['priority_level'] === "1") {
                      echo "Normal";
                    } else if ($indivTodo['priority_level'] === "2") {
                      echo "Urgent";
                    } else {
                      echo "Very Urgent";
                    }
                    ?>
                  </p>

                  <p class="card-text">Task Details: <?php echo $indivTodo['details'] ?></p>
                  <?php

                  $statusId = $indivTodo['status_id'];
                  $status_query = "SELECT * FROM statuses WHERE id = $statusId";
                  $status = mysqli_fetch_assoc(mysqli_query($conn, $status_query));

                  ?>
                  <p class="card-text">Status: <?php echo $status['name']; ?></p>
                </div>
                <div class="card-footer">
                  <!-- refactor later -->
                  <a href="controllers/process_delete_todo.php?todo_id=<?= $indivTodo['id'] ?>" class="btn
                    btn-danger my-1">Delete</a>
                  <a href="edit-todo.php?todo_id=<?= $indivTodo['id'] ?>" class="btn btn-info my-1">Edit</a>
                  <a class="btn btn-success my-1" href="
                  
                  <?php
                  if ($indivTodo['status_id'] === "1") {
                  ?>controllers/process_mark_as_done.php?todo_id=<?= $indivTodo['id'] ?><?php
                                                                                      } else {
                                                                                        ?>controllers/process_mark_as_undone.php?todo_id=<?= $indivTodo['id'] ?><?php
                                                                                                                                                              }
                                                                                                                                                                ?>
                  
                  ">

                    <?php
                    if ($indivTodo['status_id'] === "1") {
                      echo "Done!";
                    } else {
                      echo "Undo";
                    }
                    ?>
                  </a>
                </div>
              </div>
            </div>
          <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
<?php
}
?>