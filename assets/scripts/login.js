const loginBtn = document.getElementById('loginBtn');
loginBtn.disabled = true;

const emailCredInput = document.getElementById('emailCred');
const passwordCredInput = document.getElementById('passwordCred');

function isEmptyCred(value, element){
    if(value === ""){
        element.nextElementSibling.textContent = "This field is needed for log-in.";
        return true;
    }else{
        element.nextElementSibling.textContent = "";
        return false;
    }
}

function enableButtonCred(){
    if( emailCredInput.value === "" || passwordCredInput.value === "" ){
        loginBtn.disabled = true;
    }else{
        loginBtn.disabled = false;
    }
}


emailCredInput.addEventListener('blur', function(){
    const emailCredValue = emailCredInput.value;
    isEmptyCred(emailCredValue, emailCredInput);
    enableButtonCred();
});

passwordCredInput.addEventListener('blur', function(){
    const passwordCredValue = passwordCredInput.value;
    isEmptyCred(passwordCredValue, passwordCredInput);
    enableButtonCred();
});

loginBtn.addEventListener('click', function(){
    let data = new FormData;

    data.append('email', emailCredInput.value);
    data.append('password', passwordCredInput.value);

    fetch('../../controllers/process_login.php', {
        method: "POST",
        body: data,
    }).then(function(response){
        return response.text();

    }).then(function(response_from_fetch){
        if(response_from_fetch === "wrongPassword"){
            toastr['warning']("Email and Password does not match!");
        }else if(response_from_fetch === "notFound"){
            toastr['warning']("User does not exist.");
        }else{
            window.location.replace("../../missions.php");
        }
    })
});


