const registerBtn = document.getElementById('registerBtn');
registerBtn.disabled = true;

const firstNameInput = document.getElementById('firstName');
const lastNameInput = document.getElementById('lastName');
const emailInput = document.getElementById('email');
const passwordInput = document.getElementById('password');
const confirmPasswordInput = document.getElementById('confirmPassword');

function checkIfLetters(value, element){
    const letters = /^[A-Za-z]+$/;
    if(!value.match(letters)){
        element.nextElementSibling.textContent = "Numbers and special characters are not allowed in this field.";
    }else{
        element.nextElementSibling.textContent = "";
    }
}

function isEmpty(value, element){
    if(value === ""){
        element.nextElementSibling.textContent = "This field is required.";
        return true;
    }else{
        element.nextElementSibling.textContent = "";
        return false;
    }
}

function enableButton(){
    if( firstNameInput.value === "" || lastNameInput.value === "" || emailInput.value === "" || passwordInput.value === "" || (passwordInput.value !== confirmPasswordInput.value)){
        registerBtn.disabled = true;
    }else{
        registerBtn.disabled = false;
    }
}

firstNameInput.addEventListener('blur', function(){
    const firstNameValue = firstNameInput.value;
    if(!isEmpty(firstNameValue, firstNameInput)){
        checkIfLetters(firstNameValue, firstNameInput);
    }

    enableButton();
});

lastNameInput.addEventListener('blur', function(){
    const lastNameValue = lastNameInput.value;
    if(!isEmpty(lastNameValue, lastNameInput)){
        checkIfLetters(lastNameValue, lastNameInput);
    }
    enableButton();
});

emailInput.addEventListener('blur', function(){
    const emailValue = emailInput.value;
    isEmpty(emailValue, emailInput);
    enableButton();
});

passwordInput.addEventListener('blur', function(){
    const passwordValue = passwordInput.value;
    if(!isEmpty(passwordValue, passwordInput)){
        const letters = /^(?=.*?[0-9])(?=.*?[a-z])(?=.*?[A-Z])(?=.*?[^\w\s]).{8,32}$/;
        if(!letters.test(passwordValue)){
            passwordInput.nextElementSibling.innerHTML = "Password length must be 8-32. <br>Password must have at" + " least 1" + " uppercase letter, 1 lowercase" +
            " letter, 1 special character, and a number. ";
        }else{
            passwordInput.nextElementSibling.textContent = "";
        }
    }
    enableButton();
});

confirmPasswordInput.addEventListener('blur', function(){
    const confirmPasswordValue = confirmPasswordInput.value;
    if(!isEmpty(confirmPasswordValue, confirmPasswordInput)){
        if(confirmPasswordValue !== passwordInput.value){
            confirmPasswordInput.nextElementSibling.textContent = "Passwords should match";
        }else{
            confirmPasswordInput.nextElementSibling.textContent = "";
        }
    }
    enableButton();
});

registerBtn.addEventListener('click', function(){
    let data = new FormData;
    
    data.append('firstName', firstNameInput.value);
    data.append('lastName', lastNameInput.value);
    data.append('email', emailInput.value);
    data.append('password', passwordInput.value);

    fetch('../../controllers/process_register.php', {
        method: "POST",
        body: data,
    }).then(function(response){
        return response.text();
    }).then(function(response_from_fetch){
        if(response_from_fetch === "duplicate"){
            toastr['warning']("Email already used in existing account");
        }else{
            window.location.replace("../../login.php");
        }
    })
});


