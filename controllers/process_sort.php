<?php
session_start();

if (isset($_GET['sort'])) {
	if ($_GET['sort'] === 'asc') {
		$_SESSION['sortDataFromSession'] = " ORDER BY priority_level ASC";
	} else {
		$_SESSION['sortDataFromSession'] = " ORDER BY priority_level DESC";
	}
}

header("Location: " . $_SERVER['HTTP_REFERER']);
?>