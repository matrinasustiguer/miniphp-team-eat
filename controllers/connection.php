<?php
$host = "localhost";
$db_username = "root";
$db_password = "";
$db_name = "b67miniphp";

$conn = mysqli_connect($host, $db_username, $db_password, $db_name);

if (!$conn) {
    die("Connection failed: " . mysqli_error($conn));
}
