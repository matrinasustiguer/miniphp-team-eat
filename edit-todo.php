<?php
require "template/template.php";

function getTitle()
{
	echo "TaskaHolic | Edit";
}

function getContent()
{

	require "controllers/connection.php";

	$todoId = $_GET['todo_id'];

	$todo_query = "SELECT * FROM todos WHERE id = $todoId";

	$todo = mysqli_fetch_assoc(mysqli_query($conn, $todo_query));



?>

	<!--Edit-->
	<h1 class="text-center py-5">Edit Task</h1>
	<div class="d-flex justify-content-center align-items-center">

		<form action="controllers/process_edit_todo.php" method="POST" class="mb-5" enctype="multipart/form-data">
			<div class="form-group">
				<label for="title">Title: </label>
				<input type="text" name="title" class="form-control" value="<?php echo $todo['title'] ?>">
			</div>
			<div class="form-group">
			<label for="priority_level">Priority Level:</label>
              <select name="priority_level" class="form-control">
                <option value="1">Normal</option>
                <option value="2">Urgent</option>
                <option value="3">Very Urgent</option>
              </select>
			</div>
			<div class="form-group">
				<label for="details">Task Details:</label>
				<textarea name="details" class="form-control" rows="8" cols="30"><?php echo $todo['details'] ?></textarea>
			</div>

			<input type="hidden" name="status_id" value="<?php echo $todo['status_id'] ?>">

			<input type="hidden" name="todo_id" value="<?php echo $todo['id'] ?>">
			<button class="btn btn-info" type="submit">Edit Task</button>
		</form>
	</div>
<?php
}
?>