<?php session_start(); ?>
<!doctype html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

  <link rel="stylesheet" href="https://bootswatch.com/4/darkly/bootstrap.css">

  <link rel="stylesheet" href="../assets/css/styles.css">

  <link rel="icon" href="../assets/images/th-logo.png">

  <script src="https://code.jquery.com/jquery-3.5.1.min.js" defer></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js" defer></script>

  <script src="../assets/scripts/register.js" defer></script>
  
  <script src="../assets/scripts/login.js" defer></script>

  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">

  <title>
    <?php getTitle(); ?>
  </title>
</head>

<body>
  <?php require "navbar.php"; ?>

  <?php
  getContent();
  ?>

  

  <?php require "footer.php"; ?>


</body>

</html>