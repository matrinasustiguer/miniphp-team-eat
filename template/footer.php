<footer class="footer font-small bg-footer">

	<div class="d-flex justify-content-center align-items-center my-2">
		<a href="" a target=”_blank”>
			<img class="social-logo mx-1 my-2" src="./assets/images/fb-icon-th.png">
		</a>
		<a href="" a target=”_blank”>
			<img class="social-logo mx-1 my-2" src="./assets/images/twitter-th.png">
		</a>
		<a href="" a target=”_blank”>
			<img class="social-logo mx-1 my-2" src="./assets/images/insta-icon-th.png">
		</a>
	</div>

	<div class="footer-copyright text-center py-2">
		Copyright © 2020 <a href="
		<?php
		if (isset($_SESSION['user'])) {
		?>../missions.php<?php
						} else {
							?>../index.php<?php
										}
											?>
		">TaskaHolic PH</a>
	</div>
</footer>