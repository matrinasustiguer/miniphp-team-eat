TaskaHolic PH.

A website app for your to-do list. The simplest way to keep your notes.

Keep all your tasks in one place.
Add your tasks with an easy to use form. Write detailed description for each of your tasks.

Sort tasks by the priority level you assign to them. 
View your ongoing and accomplished tasks together or separately. 

And don't worry about typos or mis-clicks because you can always delete, edit, and mark as done the tasks you added.
Also, you can always "mark as undone" the done tasks with an undo button, so click your heart out.

We do need you to register so as to know you better. You need to login.
This way, your tasks are kept on your account and other users won't see it. It will be your very own space.
All you need is your name, email, and password, and you're all set.


To use:

1)  Register (if not yet registered).
2)  Login
3)  Add your tasks on the form at the right side of the screen.
4)  View your tasks on the main dashboard.
5)  Sort your tasks via priority level.
6)  View ongoing and accomplished tasks.
7)  Click Edit button to edit a task.
8)  Click Delete button to delete a task.
9)  Click Done! button to mark a task as done.
10) Click Undo button to mark a done task as undone.
11) Click TaskaHolic to quickly redirect to your tasks at the main page 
    (if you're not logged in, it will redirect you to the landing page).
11) Once you're done, you can logout.


Let's be productive today and never miss a task with TaskaHolic PH.


