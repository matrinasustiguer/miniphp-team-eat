<?php
require "template/template.php";

function getTitle()
{
  echo "TaskaHolic | Register";
}

function getContent()
{
?>
  <div id="registerContainer">
    <div class="d-flex justify-content-center align-items-center flex-column">
      <h1 class="py-5">Register</h1>
      <form action="controllers/process_register.php" method="POST">
        <div class="form-group">
          <label for="firstName">
            <span class="text-danger">*</span>First Name:
          </label>
          <input type="text" name="firstName" class="form-control" id="firstName">
          <span class="text-danger"></span>
        </div>
        <div class="form-group">
          <label for="lastName">
            <span class="text-danger">*</span>Last Name:
          </label>
          <input type="text" name="lastName" class="form-control" id="lastName">
          <span class="text-danger"></span>
        </div>
        <div class="form-group">
          <label for="email">
            <span class="text-danger">*</span>Email:
          </label>
          <input type="email" name="email" class="form-control" id="email">
          <span class="text-danger"></span>
        </div>
        <div class="form-group">
          <label for="password">
            <span class="text-danger">*</span>Password:
          </label>
          <input type="password" name="password" class="form-control" id="password">
          <span class="text-danger"></span>
        </div>
        <div class="form-group">
          <label for="confirmPassword">
            <span class="text-danger">*</span>Confirm Password:
          </label>
          <input type="password" name="confirmPassword" class="form-control" id="confirmPassword">
          <span class="text-danger"></span>

          <button type="button" class="btn btn-primary m-2 p-1" id="registerBtn">Register</button>

          <!-- <p class="py-2">Already registered? <a href="login.php">Login</a></p> -->
        </div>

      </form>

    </div>
  </div>
<?php
}
?>